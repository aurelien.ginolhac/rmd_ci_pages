README
================
A. Ginolhac
2022-02-04

and in Gitlab pages will see where

  - Create a Project Access Token with scope `write_repository`, value
    is `xxxxxx`
  - CI/CD Variables: `DEPLOY_TOKEN` with Value `xxxxxx` Protected and
    Masked.

PROJECT\_PATH wihthout extension

## Pipeline

## Preliminary steps

The tools needed are of course `R` and **RStudio** makes it even nicer.

Then, you need to install the following:

  - in **R**
      - `targets`
      - `renv`
  - in your `PATH`:
      - `google-chrome` or `chromium` for html -\> pdf with `pagedown`

Once installed, before running the first **Build**, restore the
necessary R packages in the console with:

``` r
renv::restore()
```

this will install the packages detailed in `renv.lock` in a local
enclosed library. Those R packages are needed for building the course
materials.

## Cloning this repository

Binary files (images, datasets) are tracked using [git **L**arge
**F**ile **S**torage](https://git-lfs.github.com/). If you would like to
clone this repository and didn’t setup git LFS, do it first: look at the
[installation help
page](https://github.com/git-lfs/git-lfs/wiki/Installation) on the [git
LFS website](https://git-lfs.github.com/).

To clone the repository you can use `git lfs clone` instead of `git
clone` to speed up the download process.

## Folder structure

This repository **should only contain the Rmarkdown source code**. The
site is knitted with gitlab’s continuous integration (CI). To make it
work stick to the following folder structure:

    ├── .Rprofile       # for renv
    ├── .gitattributes  # for git LFS
    ├── .gitignore 
    ├── Dockerfile      # for docker-in-docker
    ├── LICENSE
    ├── NEWS.md
    ├── README.md       # Workflow, globals and targets
    ├── renv/
    │   ├── activate.R
    │   └── settings.dcf
    ├── renv.lock
    ├── site/
    │   ├── about.Rmd
    │   ├── css
    │   ├── footer.html
    │   ├── header.html
    │   ├── img/
    │   ├── index.Rmd
    │   ├── install_tutorial.Rmd
    │   ├── lectures/   # lectures Rmd
    │   ├── lectures.Rmd
    │   ├── _output.yml
    │   ├── practicals/ # practicals Rmd
    │   ├── practicals.Rmd
    │   ├── projects/   # projects Rmd
    │   └── tips.Rmd
    └── _targets.R      # targets plan (generated automatically)

<img src="https://raw.githubusercontent.com/ropensci/targets/master/man/figures/logo.png" width="100">

## Worflow manager: `targets`

[`targets`](https://docs.ropensci.org/targets/) is a workflow manager
for R developed by **William Landau**.

Dependencies are defined by in a `plan` and depicted there:

<https://biostat1.uni.lu/preview/artifacts/targets_graph.html>

Only detected changes tell `targets` to re-run a peculiar step and its
downstream dependencies. `targets` tracks the hashes of source files, if
they change, then targets are triggered to regenerate the
html/pdf/template.

All `Rmd` files are rendered using the `rmarkdown::render_site()`
function to create the files in the `docs` folder with navbar, header
and footer.

  - Source files are organized as follows (number in file names will be
    use as index in the list):
      - `lectures` folder for `xaringan` Rmd files
      - `practicals` folder for the tutorials rendered using
        `unilur::tutorial_html()`, `unilur::tutorial_html_solution()`
        and `unilur::tutorial_html_answer()`.
      - `projects` same as for prcticals
      - Every file in these folders not starting with an underscore
        (“\_”) and not being a used source will be copied to the
        docs folder (this is a `rmarkdown::render_site()` behaviour).
        Except those listed in `exclude` in `_site.yml`

<img src="https://raw.githubusercontent.com/rstudio/renv/master/man/figures/logo.svg" width="100">

## Package manager: `renv`

[`renv`](https://rstudio.github.io/renv/articles/renv.html) is a R
package for a local dependency of packages. It works with a `renv.lock`
JSON file that contains the packages used in a project, their versions,
and origin (CRAN, GitHub, Gitlab, Bioconductor).

the function `renv::snapshot()` update the `renv.lock` file, which is
tracked with `git`. On a remote place, `renv::restore()` will then
populate a local environment with the exact same packages set.

Of note, 3 files are necessary and must be tracked:

  - `renv/activate.R` it tweaks the `libpaths` among other things
  - `renv/settings.dcf`
  - `.Rprofile` that source the previous file and ensure to be in a
    local `renv` env

## Gitlab Continuous Integration

On the local machine, we only deal with source files. The knitting,
*i.e* rendering, will be done at each commit, and if needed (determined
by `targets`) on the gitlab runner.

The `gitlab-ci.yml` defines **4** steps, building a `dind` image (if
needed), running `targets` and publishing. A more detailed description

  - building a docker-in-docker (`dind`) image, triggered by a change in
    the `Dockerfile`. Image is then cached. Important: `tag:
    shared-cache`
  - running `targets`, the steps are:
      - the docker image is fetched from the cache
      - the `renv` cache is retrieved
      - `renv` restore the lockfile
      - `make` the necessary steps
      - cache `renv` libs, `targets` files
      - upload artifacts and the website `docs`
  - publishing `docs` to **preview** <https://biostat1.uni.lu/preview>
    using `rsync` and an `alpine` tiny image
  - deploy `docs` to **production** <https://biostat1.uni.lu/>, this
    step requires a **manual** intervention (could be triggered from
    **slack**)

### Variables for SSH

In Gitlab, **two** variables must be filled out in **Settings \> CI/CD
\> Variables** (Click **Expand**, then button `Add Variable`):

  - `SSH_HOST` which is `bs2@10.240.6.222` the IP of
    `biostat2-cdc.uni.lu`
  - `SSH_PRIVATE_KEY` that contains the SSH private key `bs2_rsa`
    (contact me)

### NGINX sites

#### DNS request

Open a ticket **Publish and protect an offical website**, **Link domain
name to website ** like the one `RITM0024199`:

    Domain name: mads-r-py.uni.lu
    Server name: biostat2-cdc.uni.lu
    Backend server listens on port 80?: Yes
    Backend server listens on port 443? : Yes
    Backend server uses a valid certificate? : Yes
    Backend server performs http to https redirection?: Yes
    Backend server runs an API oriented service?: No
    The service must be accessible from inside the University?: Yes
    The service must be accessible from outside the University?: Yes

#### Site config

The pages are sync on the VM `biostat2-cdc.uni.lu`. This CentOS machine
is managed by <lcsb-sysadmins@uni.lu>.

To create a new site, copy one of the template of
`/etc/nginx/sites-available`. You need to have SSH access to this
machine.

For example, if you copy `rintro` to `newsite`, replace in `newsite`:

    server_name rintro.uni.lu;

by the custom DNS you asked to SIU (`mads-r-py` in the ticket above).
Then replace where the files are:

    root /var/www/rintro;

`rintro` replaced by the folder name you used in the gitlab-ci rsync
operations.

Then, symbolic link `sudo ln -s /etc/nginx/sites-available/newsite
/etc/nginx/sites-enabled/`

Restart `nginx`: `sudo systemctl nginx restart`

## Debugging

What is **not** run on your laptop locally, only on the gitlab runner:

  - lectures html -\> PDF
  - knit lectures.html and practicals.html which are done each time

to enable those actions locally, un-comment this line in `_targets.R`

`#CI <- TRUE`

To avoid `targets` failing on error, un-comment this line too:

`#tar_option_set(workspace_on_error = TRUE)`

Interestingly the conversion to PDF by `pagedown::chrome_print()` is
failing when an image is missing. That allows to catch missing that
could hidden in the html.

In case of issue, you can run

``` r
targets::tar_destroy(destroy = "all")
```

to start from scratch with `targets` cache.

Alternatively, removing the folder `docs` remove all output files
produced by the pipeline. But it keeps the `targets` meta folder.

## Enhancements

  - The `alpine` mini image used for `rsync` is missing `ssh` and
    `rsync` that are installed for every deployment. Would be better to
    have an image ready, register to the Gitlab docker registry.
  - Gitlab pages could be used. That can be done with a custom DNS is
    `http://micmac.lcsb.uni.lu/` -\> `https://isc.pages.uni.lu/micmac/`

## Previous editions

From historical times:

  - 2022: first iteration during BASV 4th semester
