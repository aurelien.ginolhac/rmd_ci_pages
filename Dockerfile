FROM rocker/r-ubuntu:20.04

LABEL org.opencontainers.image.authors="Aurelien Ginolhac <aurelien.ginolhac@uni.lu>"


RUN  apt-get update && \
  apt-get install -y --allow-downgrades --fix-missing \
  --allow-remove-essential --allow-change-held-packages \
  --allow-unauthenticated --no-install-recommends --no-upgrade \
  curl netbase \
  zip git \
  libxml2-dev \
  gpg-agent gnupg \
  libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev \
  libfontconfig1-dev \
  libcairo2-dev \
  pandoc \
  libharfbuzz-dev libfribidi-dev \
  && rm -rf /var/lib/apt/lists/* 

RUN  install.r renv
